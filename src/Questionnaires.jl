# ===========================================================
# MODULE Questionnaires
# ---------------
# included functions:
# - simpleEHI (handedness based on Edinburgh Hadedness Inventory)
# - imaginationScore
# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================
module Questionnaires
using Exp_helper, Printf

# ===========================================================
# function definitions
# ===========================================================
function simpleEHI()
   I = String[]
   push!(I, Exp_helper.get_input("Mit welcher Hand schreiben Sie?", "string", ["r"; "l"]))
   push!(I, Exp_helper.get_input("In welcher Hand halten Sie beim essen den Löffel?", "string", ["r"; "l"]))
   push!(I, Exp_helper.get_input("Mit welcher Hand halten Sie ihre Zahnbürste?", "string", ["r"; "l"]))
   push!(I, Exp_helper.get_input("Mit welcher Hand entzünden Sie ein Streichholz?", "string", ["r"; "l"]))
   push!(I, Exp_helper.get_input("Mit welcher Hand nutzen Sie ein Radiergummi?", "string", ["r"; "l"]))
   push!(I, Exp_helper.get_input("In welcher Hand halten Sie eine Nadel beim nähen?", "string", ["r"; "l"]))
   push!(I, Exp_helper.get_input("In welcher Hand halten Sie das Messer, wenn Sie ein Brot bestreichen?", "string", ["r"; "l"]))
   push!(I, Exp_helper.get_input("Mit welcher Hand benutzen Sie einen Hammer?", "string", ["r"; "l"]))
   push!(I, Exp_helper.get_input("In welcher Hand halten Sie den Schäler wenn Sie einen Apfel schälen?", "string", ["r"; "l"]))
   push!(I, Exp_helper.get_input("Mit welcher Hand zeichnen Sie?", "string", ["r"; "l"]))

   rr = findall(x->x=="r",I)
   if isempty(rr)
       hscore = 0
   else
       hscore = (length(rr)/10)*100
   end

   if hscore >= 70
       hand = "right" * string(hscore)
   elseif hscore <= 30
       hand = "left" * string(hscore)
   elseif hscore > 30 && hscore < 70
       hand = "ambidextrous" * string(hscore)
   end
   
   return hand
end

function imaginationScore()
    resp = Exp_helper.get_input("Auf einer Skala von 1-10, wie gut konnten Sie sich die Bewegung in der Imaginationsbedingung vorstellen?", ["1"; "2"; "3"; "4"; "5"; "6"; "7"; "8"; "9"; "10";])
    return resp
end

end # module
